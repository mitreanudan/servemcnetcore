﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace ServeMCNetCore.FileUtils
{
    public class ConfigFile
    {
        public string FilePath { get; set; } = "";
        public bool LiveUpdate { get; set; } = false;
        public bool IsOpen { get; private set; } = false;
        private string _rawContent = "";
        private ConcurrentDictionary<string, string> _values;

        #region DEBUG
#if DEBUG
        public ConcurrentDictionary<string, string> __debugvalues { get => _values; }
        public string __debugrawcontent { get => _rawContent; }
#endif
        #endregion

        public ConfigFile() { }
        public ConfigFile(string FilePath, bool LiveUpdate = false)
        {
            Open(FilePath);
        }

        private Dictionary<string, string> ReadValues(string[] Content)
        {
            string readFromReport = string.IsNullOrEmpty(FilePath) ? "internal" : FilePath;

            var values = new Dictionary<string, string>();
            var validKeyMatch = new Regex(@"\b[A-Za-z][A-Za-z0-9]+\b");
            var readKeys = new List<string>();

            foreach (string line in Content)
            {
                if (line.StartsWith("//") || line.StartsWith("#") || string.IsNullOrEmpty(line))
                    continue;

                string[] split = line.Split('=', 2);

                if (split.Length < 2)
                    continue;

                if (validKeyMatch.Match(split[0]).Success)
                {
                    if (readKeys.Contains(split[0]))
                        continue;

                    DebugProxy.Log("Reading {0} from {1}", split[0], readFromReport);

                    readKeys.Add(split[0]);
                    values.Add(split[0], split[1]);

                    _rawContent += split[0] + "=" + split[1] + Environment.NewLine;
                }
            }

            return values;
        }

        public void Open(string Path)
        {
            FilePath = Path;
            try
            {
                _rawContent = Utilities.GetFileText(Path);
            }
            catch
            {
                DebugProxy.Log("Unable to open config file {0}.", Path);
                return;
            }

            ReadText(_rawContent);
        }

        public void Create()
        {
            Create(FilePath);
        }

        public void Create(string Path)
        {
            if (IsOpen)
            {
                DebugProxy.Log("Cannot create a config that already exists.");
                return;
            }

            Utilities.CreateFileText(Path, "");
            _rawContent = "";
            ReadText(_rawContent);
            DebugProxy.Log("Created config file {0}.", Path);
        }

        public void ReadText(string Text)
        {
            if (Text == null)
                return;

            _rawContent = Text;
            string[] content;
            if (!_rawContent.EndsWith(Environment.NewLine))
                _rawContent += Environment.NewLine;

            content = _rawContent.Split(Environment.NewLine);

            var values = ReadValues(content);

            _values = new ConcurrentDictionary<string, string>(values);
            IsOpen = true;
        }

        private string Get(string Key)
        {
            if (!IsOpen)
            {
                DebugProxy.Log("Cannot read key {0} from config file {1} because it is not open.", Key, FilePath);
                return null;
            }

            bool success = _values.TryGetValue(Key, out string Value);

            if (!success)
                return null;
            return Value;
        }

        private void Set(string Key, string Value)
        {
            if (!IsOpen)
            {
                DebugProxy.Log("Cannot set key {0} from config file {1} because it is not open.", Key, FilePath);
                return;
            }

            _values.AddOrUpdate(Key, Value, (arg1, arg2) => Value);

            if (LiveUpdate)
                Write();
        }

        public void Write()
        {
            if (FilePath == "")
                return;

            string rawContent = "";
            foreach (var entry in _values)
                rawContent += entry.Key + "=" + entry.Value + Environment.NewLine;

            Utilities.WriteFileText(FilePath, rawContent);
        }

        public string this[string Key]
        {
            get => this.Get(Key);
            set => this.Set(Key, value);
        }

        public ICollection<string> AllKeys()
        {
            return _values.Keys;
        }
    }
}