﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("ServeMCNetCore")]
namespace ServeMCNetCore.FileUtils
{
    internal static class DebugProxy
    {
        public delegate void DebugLogDelegate(string text, params object[]? format);

        public static DebugLogDelegate LogFunction { get; set; }

        internal static void Log(string text)
        {
            LogFunction?.Invoke(text);
        }

        internal static void Log(string text, params object[] format)
        {
            LogFunction?.Invoke(text, format);
        }
    }
}
