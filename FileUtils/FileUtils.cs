﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace ServeMCNetCore
{
    namespace FileUtils
    {
        public static class Utilities
        {
            // Set to ServeMCNetCore.Properties.Resources.ResourceManager
            // This class accesses the main project's resources, not its own
            public static System.Resources.ResourceManager ResourceManager { internal get; set; }

            public static void CreateFileText(string path, string content)
            {
                using (FileStream fs = File.Create(path))
                    fs.Write(Encoding.ASCII.GetBytes(content));
            }

            public static void CreateFileFromBytes(string path, byte[] content)
            {
                using (FileStream fs = File.Create(path))
                    fs.Write(content);
            }

            public static string GetFileText(string path)
            {
                return File.ReadAllText(path);
            }

            public static byte[] GetFileBytes(string path)
            {
                return File.ReadAllBytes(path);
            }

            public static string[] GetFileLines(string path)
            {
                return File.ReadAllLines(path);
            }

            public static void WriteFileText(string path, string content)
            {
                File.WriteAllText(path, content);
            }

            public static bool IsInQuotes(string content, int index)
            {
                return false;
            }

            public static string ReadAssemblyResourceString(string key)
            {
                return ResourceManager.GetString(key);
            }

            public static string ReadAssemblyResourceFileText(string filename)
            {
                object content = ResourceManager.GetObject(filename);
                if (content == null)
                {
                    DebugProxy.Log("Could not find internal resource {0}.", filename);
                    return null;
                }
                return (string)content;
            }

            public static byte[] ReadAssemblyResourceFileBytes(string filename)
            {
                byte[] content;
                using (var read = ResourceManager.GetStream(filename))
                using (var memStream = new MemoryStream())
                {
                    if (read == null)
                    {
                        DebugProxy.Log("Could not find internal resource {0}.", filename);
                        return new byte[] { };
                    }

                    read.CopyTo(memStream);
                    content = memStream.ToArray();
                }

                return content;
            }

            public static bool FileExists(string filename)
            {
                return File.Exists(filename);
            }

            public static bool DirectoryExists(string directory)
            {
                return Directory.Exists(directory);
            }

            public static void CreateDirectory(string directory)
            {
                Directory.CreateDirectory(directory);
            }

            /// <summary>
            /// Returns full paths of subdirectories of <paramref name="directory"/> relative to current directory.
            /// </summary>
            /// <param name="directory"></param>
            /// <returns></returns>
            public static string[] SubdirectoriesOf(string directory)
            {
                return Directory.GetDirectories(directory);
            }

            public static bool EnsureDirectory(string directory)
            {
                if (DirectoryExists(directory))
                    return true;

                CreateDirectory(directory);
                return false;
            }

            public static long GetFileSize(string filename)
            {
                return new FileInfo(filename).Length;
            }

            public static bool MoveFile(string from, string to)
            {
                try
                {
                    File.Move(from, to);
                } catch { return false; }
                return true;
            }

            /// <summary>
            /// Creates a copy of a file. If the destination file already exists
            /// then it is deleted and replaced.
            /// </summary>
            /// <param name="from"></param>
            /// <param name="to"></param>
            /// <returns></returns>
            public static void CopyFile(string from, string to)
            {
                if (FileExists(to))
                    File.Delete(to);
                File.Copy(from, to);
            }
        }
    }
}
