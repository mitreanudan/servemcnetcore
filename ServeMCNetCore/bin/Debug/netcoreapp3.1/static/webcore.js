(function(){
    var query = new URLSearchParams(window.location.search);

    if (query.has('action'))
        $smc.loadAction(query.get('action'));
    else
        $smc.loadAction('main');

    $('nav ul li').click(function() {
        var action = $(this).attr('data-action');
        $smc.loadAction(action);
    });
})();