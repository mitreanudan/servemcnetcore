var $smc = {
    mcRunning: false,

    mainUrl: 'http://' + window.location.host + '/',
    
    getUrlFromRoute: function(route) {
        if (route.startsWith('/'))
            route = route.substring(1, route.length);
        return this.mainUrl + route;
    },

    loadAction: function(action) {
        $.ajax({
            type: 'get',
            url: $smc.mainUrl + 'action/' + action + '.html',
            success: function(data) {
                $('nav ul li').each(function() {
                    $(this).removeClass('selected');
                });
                $('main').html(data);
                var dataName = $('main #actionName').val();
                $(`[data-action=${dataName}]`).addClass('selected');
                var pageStill = $('main #actionPageStill').val();
                if (pageStill === 'true') {
                    $('main').addClass('main-still');
                    $('section').addClass('section-still');
                }
                else {
                    $('main').removeClass('main-still');
                    $('section').removeClass('section-still');
                }
            },
            error: function(data) {
                $('main').html('<h1>Could not load page. Check if ServeMC is still running.</h1>');
            }
        });
    },

    loadOverlay: function(url) {
        $.ajax({
            type: 'get',
            url: url,
            success: function(data) {
                var div = $('<div class="overlayDiv"></div>');
                $(div)
                    .html(data)
                    .click((e) => e.stopPropagation());
                $('#overlay')
                    .css('display', 'flex')
                    .append(div)
                    .click(() => $smc.closeOverlay());
            },
            error: function() {
                console.error(`Could not load ${url}. Please check if ServeMC is still running.`);
            }
        });
    },

    closeOverlay: function() {
        $('#overlay')
            .css('display', 'none')
            .children().remove();
    },

    console_data: [],
    clearConsole: function() {
        this.console_data = [];
    },
    
    serverStatusData: {
        'stopped': {
            isCard: true,
            name: 'stopped',
            warning: 2,
            title: 'Restart server',
            message: 'Server has stopped. Would you like to restart it?',
            action: '/engine?action=restart-server'
        },
        'eula_fault': {
            isCard: true,
            name: 'eula_fault',
            warning: 2,
            title: 'EULA',
            message: 'You must accept Mojang\'s EULA in order to run the server.',
            action: '/engine?action=open-eula',
            displayRestart: true,
            persist: true,
            once: true
        },
        'network_contact_success': {
            isCard: false,
            name: 'network_contact_success'
        }
    },

    randomStr: function(length) {
        // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
        if (!length)
            length = 8;
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
};