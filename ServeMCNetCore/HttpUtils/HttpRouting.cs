﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Reflection;

namespace ServeMCNetCore.HttpUtils
{
    public static class HttpRouting
    {
        public static bool LogRoutes = false;

        private delegate HttpResponse RouteHandler(HttpRequest req);

        static readonly Dictionary<string, RouteHandler> Routes = new Dictionary<string, RouteHandler>
        {
            { "", Controllers.MainController.Index },
            { "/static", Controllers.Controller.WebStatic },
            { "/action", Controllers.ServeMcController.Action },
            { "/engine", Controllers.ServeMcController.Engine },
            { "/engine/polloutput", Controllers.ServeMcController.PollOutput }

            #region DEBUG
            /*
             * Routes /gc to Garbage Collector, calling this
             * method in production throws.
             */
#if DEBUG
            , { "/gc", Controllers.MainController._gcforce },
            { "/test", Controllers.MainController.Test }
#endif
            #endregion
        };

        public static async Task<HttpListenerResponse> HandleRoute(HttpListenerContext ctx, bool close = true)
        {
            var request = new HttpRequest(ctx.Request);
            string route = request.Route;

            RouteHandler handler;

            if (App.Session.ConfigCache["servemc"]["logRoutes"] == "true")
                App.Session.Log("{0} \"{1}\" Route=\"{2}\" Action=\"{3}\"", request.Method.ToString().ToUpper(), request.FullPath, route, request.Action);

            if (!Routes.ContainsKey(route))
            {
                handler = Controllers.Controller.E404;
            }
            else
            {
                handler = Routes[route];
            }

            HttpResponse response;
            
            try
            {
                // this is the shit
                response = handler(request);
            }
            catch (Exception ex)
            {
                string source = string.IsNullOrEmpty(ex.Source) ? "???" : ex.Source;
                string method = ex.TargetSite == null ? "???" : ex.TargetSite.Name;
                string trace = string.IsNullOrEmpty(ex.StackTrace) ? "???" : ex.StackTrace;

                string errorInfo = string.Format("StackTrace: {0}\nSource: {1}\nMethod: {2}\nMessage: {3}", trace, source, method, ex.Message);

                ex.Data["err"] = "HTTP request handling exception.";
                App.Session.ReportException(ex);

                response = new HttpResponse("500 Internal Server Error\n" + errorInfo)
                {
                    StatusCode = 500,
                    ContentType = HttpResponse.Type.Text
                };
            }

            foreach (var header in response.Headers)
                ctx.Response.AddHeader(header.Key, header.Value);

            foreach (var cookie in response.Cookies)
                ctx.Response.SetCookie(new Cookie
                {
                    Name = cookie.Key,
                    Value = cookie.Value,
                    HttpOnly = true
                });

            foreach (var cookie in response.HttpCookies)
                ctx.Response.SetCookie(cookie);

            ctx.Response.StatusCode = response.StatusCode;
            ctx.Response.ContentType = HttpResponse.ContentTypeStrings[response.ContentType];
            ctx.Response.ContentEncoding = Encoding.UTF8;
            await ctx.Response.OutputStream.WriteAsync(response.GetByteContent());

            if (close && response.CloseResponse)
                ctx.Response.Close();

            return ctx.Response;
        }
    }
}
