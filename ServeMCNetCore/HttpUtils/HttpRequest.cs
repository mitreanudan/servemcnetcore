﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace ServeMCNetCore.HttpUtils
{
    public class HttpRequest
    {
        public enum Type
        {
            Html,
            Json,
            FormData,
            Text,
            Css,
            Javascript,
            Png,
            Jpeg,
            Svg,
            Otf,
            Ttf
        };
        private static Dictionary<string, Type> ContentTypeMap = new Dictionary<string, Type>()
        {
            { "text/html", Type.Html },
            { "text/json", Type.Json },
            { "multipart/form-data", Type.FormData },
            { "text/plain", Type.Text },
            { "text/css", Type.Css },
            { "text/javascript", Type.Javascript },
            { "image/png", Type.Png },
            { "image/jpeg", Type.Jpeg },
            { "image/svg+xml", Type.Svg },
            { "application/x-font-otf", Type.Otf },
            { "application/x-font-ttf", Type.Ttf }
        };

        private readonly Dictionary<string, string> _query = new Dictionary<string, string>();

        public enum MethodType { Get, Post, Put, Delete };
        public MethodType Method { get; private set; }
        public Type ContentType { get; private set; }
        public string ContentTypeString { get; private set; }
        private string Boundary { get; set; }
        public string FullPath { get; private set; }
        public string Path { get; private set; }
        public string Route { get; private set; }
        public string Action { get; set; }
        public string QueryString { get; private set; }
        public byte[] Body { get; private set; }
        public string BodyText { get; private set; }
        public bool HasBody { get => Body != null && Body.Length != 0; }
        public Encoding ContentEncoding { get; private set; }

        internal static HttpRequest Empty = EmptyRequest();

        private HttpRequest() { }

        public HttpRequest(HttpListenerRequest req)
        {
            ContentTypeString = req.ContentType;

            try
            {
                IdentifyContentType();
            }
            catch (Exception ex)
            {
                App.Session.ReportException(ex);
            }

            FullPath = req.Url.OriginalString;

            Path = req.Url.AbsolutePath;
            Path = Path.EndsWith('/') ? Path.Substring(0, Path.Length - 1) : Path;
            Route = Path;

            var splitPath = Path.Split('/');
            if (splitPath[splitPath.Length - 1].Contains('.') && splitPath.Length > 1)
            {
                Action = splitPath[splitPath.Length - 1];
                Route = Path.Substring(0, Path.Length - Action.Length - 1);
            }

            QueryString = req.Url.Query;
            QueryString = QueryString.StartsWith('?') ? QueryString.Substring(1, QueryString.Length - 1) : QueryString;

            switch (req.HttpMethod)
            {
                case "GET":
                    Method = MethodType.Get;
                    break;
                case "POST":
                    Method = MethodType.Post;
                    break;
                case "PUT":
                    Method = MethodType.Put;
                    break;
                case "DELETE":
                    Method = MethodType.Delete;
                    break;
                default:
                    Method = MethodType.Get;
                    break;
            }

            if (!String.IsNullOrEmpty(QueryString))
            {
                string[] queries = QueryString.Split('&');
                foreach (var q in queries)
                {
                    string[] values = q.Split('=', 2);
                    if (values.Length < 2)
                        continue;
                    _query.Add(values[0], values[1]);
                }
            }

            if (req.HasEntityBody && (Method == MethodType.Post || Method == MethodType.Put))
            {
                using (Stream body = req.InputStream)
                using (MemoryStream memStream = new MemoryStream())
                {
                    body.CopyTo(memStream);
                    Body = memStream.ToArray();
                }

                ContentEncoding = req.ContentEncoding;
            }
        }

        private void IdentifyContentType()
        {
            if (string.IsNullOrEmpty(ContentTypeString))
                return;

            // recreate contentType, from "multipart/form-data; boundary=XXX" to "multipart/form-data"
            string[] frags = ContentTypeString.Split(';');
            // remove first spaces
            for (int i = 0; i < frags.Length; i++)
                for (int j = 0; j < frags[i].Length; j++)
                    if (frags[i][j] != ' ')
                    {
                        frags[i] = frags[i].Substring(j);
                        break;
                    }

            if (!ContentTypeMap.ContainsKey(frags[0]))
                ContentType = Type.Text;
            else
                ContentType = ContentTypeMap[frags[0]];
        }

        private static HttpRequest EmptyRequest()
        {
            return new HttpRequest();
        }

        public string GetQuery(string query)
        {
            if (!_query.ContainsKey(query))
                return null;
            else
                return _query[query];
        }
    }
}
