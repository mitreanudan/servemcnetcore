﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ServeMCNetCore.HttpUtils
{
    public class HttpResponse
    {
        public enum Type { 
            Html, 
            Json, 
            FormData, 
            Text, 
            Css, 
            Javascript, 
            Png, 
            Jpeg, 
            Svg,
            Otf,
            Ttf
        };
        public static Dictionary<Type, string> ContentTypeStrings = new Dictionary<Type, string>()
        {
            { Type.Html, "text/html" },
            { Type.Json, "text/json" },
            { Type.FormData, "multipart/form-data" },
            { Type.Text, "text/plain" },
            { Type.Css, "text/css" },
            { Type.Javascript, "text/javascript" },
            { Type.Png, "image/png" },
            { Type.Jpeg, "image/jpeg" },
            { Type.Svg, "image/svg+xml" },
            { Type.Otf, "application/x-font-otf" },
            { Type.Ttf, "application/x-font-ttf" }
        };

        public Type ContentType { get; set; } = Type.Html;
        public int StatusCode { get; set; } = 200;
        public bool CloseResponse { get; set; } = true;
        private byte[] _content;
        public object Content
        {
            get => _content;
            set
            {
                if (value == null)
                    return;
                if (ContentType == Type.Html)
                    _content = Encoding.UTF8.GetBytes((string)value);
                else if (ContentType == Type.Json)
                    _content = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(value));
                else
                    _content = (byte[])value;
            }
        }

        internal Dictionary<string, string> Headers { get; set; }
        internal Dictionary<string, string> Cookies { get; set; }
        internal List<System.Net.Cookie> HttpCookies { get; set; }

        public HttpResponse()
        {
            Headers = new Dictionary<string, string>();
            Cookies = new Dictionary<string, string>();
            HttpCookies = new List<System.Net.Cookie>();
        }

        public HttpResponse(string content) : this()
        {
            ContentType = Type.Html;
            Content = content;
        }

        public byte[] GetByteContent()
        {
            return (byte[])Content;
        }

        public void AddHeader(string header, string value)
        {
            Headers.Add(header, value);
        }

        public void AddCookie(string cookie, string value)
        {
            Cookies.Add(cookie, value);
        }

        public void AddCookie(System.Net.Cookie cookie)
        {
            HttpCookies.Add(cookie);
        }

        public void SetContentBytes(byte[] content)
        {
            _content = content;
        }
    }
}
