﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace ServeMCNetCore.HttpUtils.Controllers
{
    public class ServeMcController : Controller
    {
        public static HttpResponse Action(HttpRequest req)
        {
            return WebStatic(req);
        }

        public static HttpResponse Engine(HttpRequest req)
        {
            HttpRequest.MethodType method = req.Method;

            switch (req.GetQuery("action"))
            {
                case "":
                    return BadRequest();

                // GET 
                case "get-servers":
                    if (method != HttpRequest.MethodType.Get)
                        return MethodNotAllowed();

                    var serverConfigList = new List<ServeMCNetCoreEngine.ServerPackageConfig>();
                    if (!App.Session.ServerLibrary.Empty)
                        foreach (var server in App.Session.ServerLibrary.Servers)
                            serverConfigList.Add(server.GetConfig());

                    return JsonResponse(serverConfigList);

                // GET
                case "get-server":
                    if (method != HttpRequest.MethodType.Get)
                        return MethodNotAllowed();

                    string serverId = req.GetQuery("id");
                    if (string.IsNullOrEmpty(serverId))
                        return BadRequest("Missing server id query.");

                    string serverJson = App.Session.ServerLibrary.GetServerJson(serverId);

                    if (string.IsNullOrEmpty(serverJson))
                        return NotFound();
                    return JsonResponse(serverJson);

                // POST
                case "add-server":
                    if (method != HttpRequest.MethodType.Post)
                        return MethodNotAllowed();

                    if (!req.HasBody)
                        return BadRequest("Empty request body.");

                    HttpFormData addServerPost = new HttpFormData(req);
                    if (addServerPost.KeyCount != 2)
                        return BadRequest("Bad request body.");

                    string serverConfigJson = addServerPost[0].Text;
                    byte[] serverJar = addServerPost[1].Content;
                    string createId = App.Session.ServerLibrary.CreateServer(serverConfigJson, serverJar);
                    return JsonResponse(new { id = createId });

                // POST
                case "check-servertypejar":
                    if (method != HttpRequest.MethodType.Post)
                        return MethodNotAllowed();

                    if (!req.HasBody)
                        return BadRequest("Empty request body.");

                    var serverTypePost = new HttpFormData(req);
                    if (serverTypePost[0] == null)
                        return BadRequest("Could not find content in request body.");

                    var serverType = ServeMCNetCoreEngine.ServerPackage.CheckProviderFromJar(serverTypePost[0].Content);
                    return JsonResponse(new { serverType });

                // GET
                case "start-server":
                    if (method != HttpRequest.MethodType.Get)
                        return MethodNotAllowed();

                    string startId = req.GetQuery("id");
                    if (string.IsNullOrEmpty(startId))
                        return BadRequest("Missing server id from query.");

                    bool success = App.Session.ServerLibrary.StartServer(startId);
                    return Content(new { success });

                // GET
                case "get-console":
                    if (method != HttpRequest.MethodType.Get)
                        return MethodNotAllowed();

                    if (App.Session.ServerLibrary.ActiveInstance == null)
                        return Conflict("Cannot get console contents while no server is running.");

                    return Content(new { console = App.Session.ServerLibrary.ActiveInstance.ConsoleContent });

                // GET
                case "open-eula":
                    if (method != HttpRequest.MethodType.Get)
                        return MethodNotAllowed();

                    string eulaPath = System.IO.Path.Combine(App.Session.ServerLibrary.ActiveInstance.ServerDirectory, "eula.txt");
                    System.Threading.Tasks.Task.Run(() =>
                    {
                        App.Session.Debug(eulaPath);
                        new System.Diagnostics.Process()
                        {
                            StartInfo = new System.Diagnostics.ProcessStartInfo(eulaPath)
                            {
                                UseShellExecute = true
                            }
                        }.Start();
                    });
                    return OK();

                // GET
                case "restart-server":
                    if (method != HttpRequest.MethodType.Get)
                        return MethodNotAllowed();

                    if (App.Session.ServerLibrary.ActiveInstance == null)
                        return Content(new { error = "No server instance to restart." });

                    if (!App.Session.ServerLibrary.ActiveInstance.Stopped) ;
                        // stop server then restart it, stop safe?

                    App.Session.ServerLibrary.ActiveInstance.Start();
                    return Content(new { success = true });

                // POST
                case "send-command":
                    if (method != HttpRequest.MethodType.Post)
                        return MethodNotAllowed();

                    if (App.Session.ServerLibrary.ActiveInstance == null)
                        return Content(new { error = "No server instance to send command to." });

                    if (App.Session.ServerLibrary.ActiveInstance.Stopped)
                        return Content(new { error = "Server instance is not running, cannot send command." });

                    var serverCmdPost = new HttpFormData(req);
                    if (serverCmdPost["command"] == null)
                        return BadRequest("Missing command field from FormData.");

                    App.Session.ServerLibrary.ActiveInstance.SendInput(serverCmdPost["command"].Text);

                    return OK();

                // GET
                case "is-running":
                    if (method != HttpRequest.MethodType.Get)
                        return MethodNotAllowed();

                    return Content(new { running = App.Session.ServerLibrary.ActiveInstance.Running });

                // GET, POST, DELETE, PUT
                default:
                    return BadRequest();
            }
        }

        public static HttpResponse PollOutput(HttpRequest req)
        {
            if (App.Session.ServerLibrary.ActiveInstance == null)
                return Conflict("Cannot poll output of server instance while it does not exist.");

            while (App.Session.ServerLibrary.ActiveInstance.GetOutput(out string stdout, out string stderr, out string status))
                return Content(new { stdout, stderr, status });

            if (App.Session.ServerLibrary.ActiveInstance.Stopped)
                return Content(new { error = "stopped" });

            return Content(new { error = "eof" });
        }
    }
}
