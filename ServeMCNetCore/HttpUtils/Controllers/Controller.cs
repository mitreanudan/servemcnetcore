﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace ServeMCNetCore.HttpUtils.Controllers
{
    public class Controller
    {
        public Controller() { }

        public static HttpResponse WebStatic(HttpRequest req)
        {
            byte[] content;
#if DEBUG
            string path = System.IO.Path.Combine("static/", req.Action);
            App.Session.Debug("Reading static from disk: \"{0}\"", path);

            try
            {
                content = FileUtils.Utilities.GetFileBytes(path);
            }
            catch (Exception ex)
            {
                return E404(req);
            }
#else
            content = FileUtils.Utilities.ReadAssemblyResourceFileBytes(req.Action);
#endif
            var response = new HttpResponse();

            response.SetContentBytes(content);
            int extI = req.Action.LastIndexOf('.');
            string extension = req.Action.Substring(extI + 1, req.Action.Length - extI - 1);

            switch (extension)
            {
                case "css":
                    response.ContentType = HttpResponse.Type.Css;
                    break;
                case "js":
                    response.ContentType = HttpResponse.Type.Javascript;
                    break;
                case "png":
                    response.ContentType = HttpResponse.Type.Png;
                    break;
                case "jpg":
                    response.ContentType = HttpResponse.Type.Jpeg;
                    break;
                case "svg":
                    response.ContentType = HttpResponse.Type.Svg;
                    break;
                case "otf":
                    response.ContentType = HttpResponse.Type.Otf;
                    break;
                case "ttf":
                    response.ContentType = HttpResponse.Type.Ttf;
                    break;

            }

            return response;
        }

        public static HttpResponse StatusCode(int code)
        {
            return new HttpResponse()
            {
                StatusCode = code
            };
        }

        public static HttpResponse OK()
        {
            return new HttpResponse();
        }

        public static HttpResponse E404()
        {
            return new HttpResponse("Error 404")
            {
                StatusCode = 404
            };
        }

        public static HttpResponse E404(HttpRequest req)
        {
            return new HttpResponse("Error 404")
            {
                StatusCode = 404
            };
        }

        public static HttpResponse NotFound()
        {
            return E404();
        }

        public static HttpResponse JsonResponse(string json)
        {
            return new HttpResponse(json)
            {
                ContentType = HttpResponse.Type.Json
            };
        }

        public static HttpResponse JsonResponse(object obj)
        {
            string json = JsonSerializer.Serialize(obj);
            return JsonResponse(json);
        }

        public static HttpResponse BadRequest()
        {
            return new HttpResponse("400 Bad Request")
            {
                StatusCode = 400,
                ContentType = HttpResponse.Type.Text
            };
        }

        public static HttpResponse BadRequest(string message)
        {
            return new HttpResponse(message)
            {
                StatusCode = 400,
                ContentType = HttpResponse.Type.Text
            };
        }

        public static HttpResponse MethodNotAllowed()
        {
            return new HttpResponse("400 Method Not Allowed")
            {
                StatusCode = 405,
                ContentType = HttpResponse.Type.Text
            };
        }

        public static HttpResponse MethodNotAllowed(string message)
        {
            return new HttpResponse(message)
            {
                StatusCode = 405,
                ContentType = HttpResponse.Type.Text
            };
        }

        public static HttpResponse Conflict()
        {
            return new HttpResponse()
            {
                StatusCode = 409,
                ContentType = HttpResponse.Type.Text
            };
        }

        public static HttpResponse Conflict(string message)
        {
            return new HttpResponse(message)
            {
                StatusCode = 409,
                ContentType = HttpResponse.Type.Text
            };
        }

        /// <summary>
        /// Creates a text/plain string response or a JSON application/json response
        /// depending on the <paramref name="content"/> value.
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static HttpResponse Content(object content)
        {
            if (content is string)
                return new HttpResponse((string)content)
                {
                    ContentType = HttpResponse.Type.Text
                };

            string jsonContent = JsonSerializer.Serialize(content);
            return new HttpResponse(jsonContent)
            {
                ContentType = HttpResponse.Type.Json
            };
        }
    }
}
