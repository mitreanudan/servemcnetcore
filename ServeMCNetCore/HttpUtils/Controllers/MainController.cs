﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using NetworkTransfer = ServeMCNetCoreEngine.ServerNetworkTransfer;

namespace ServeMCNetCore.HttpUtils.Controllers
{
    public class MainController : Controller
    {
        public static HttpResponse Index(HttpRequest req)
        {
            req.Action = "index.html";

            return WebStatic(req);
        }

        // /test
        public static HttpResponse Test(HttpRequest req)
        {
            if (App.Session._ISDEBUG)
            {
                // <TEST CODE>

                string testPath = "C:\\Users\\Dan Mitreanu\\source\\repos\\ServeMCNetCore\\ServeMCNetCore\\bin\\Debug\\netcoreapp3.1\\servers\\6aba1a74-bec5-4646-a064-557eec1c1d2b";
                string propPath = System.IO.Path.Combine(testPath, "server.properties");
                var prop = new FileUtils.ConfigFile(propPath);

                return Content(prop["server-port"]);

                // </TEST CODE>
            }

            return NotFound();
        }

        /*
         * Force garbage collection.
         */ 
        public static HttpResponse _gcforce(HttpRequest req)
        {
            if (App.Session._ISDEBUG)

                GC.Collect();

            else
            {
                var ex = new Exception("Garbage collection force trigger attempt.");
                ex.Data["err"] = "Do not force garbage collection in production.";
                throw ex;
            }

            return NotFound();
        }
    }
}
