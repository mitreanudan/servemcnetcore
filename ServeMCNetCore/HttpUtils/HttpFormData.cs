﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace ServeMCNetCore.HttpUtils
{
    public class HttpFormData
    {
        public class FormDataFragment
        {
            public string ContentDisposition { get; private set; }
            public string Name { get; private set; }
            public string ContentType { get; private set; }
            public byte[] Content { get; private set; }
            public string Text { get => Encoding.GetString(Content); }
            public bool IsFile { get; private set; }
            public string FileName { get; set; }
            public Encoding Encoding { get; private set; }

            protected FormDataFragment(byte[] fragment, Encoding encoding = null)
            {
                Encoding = encoding ?? Encoding.ASCII;

                var metaStrings = new List<string>();

                int start = 0;
                for (int i = 0; i < fragment.Length - 4; i++)
                    if (fragment[i] == 13 && fragment[i + 1] == 10)
                    {
                        bool finishMeta = false;
                        if (fragment[i + 2] == 13 && fragment[i + 3] == 10)
                            // \r \n \r \n, that means content starts
                            finishMeta = true;

                        var line = new ArraySegment<byte>(fragment, start, i - start).ToArray();
                        metaStrings.Add(Encoding.GetString(line));
                        
                        start = i + 2;

                        if (finishMeta)
                        {
                            start += 2;
                            break;
                        }
                    }

                Content = new ArraySegment<byte>(fragment, start, fragment.Length - start).ToArray();

                const string cDisposition = "Content-Disposition: ";
                const string cType = "Content-Type: ";
                const string cName = " name=";
                const string cFilename = " filename=";

                foreach (var str in metaStrings)
                    if (str.StartsWith(cDisposition))
                        ContentDisposition = str.Substring(cDisposition.Length);
                    else if (str.StartsWith(cType))
                        ContentType = str.Substring(cType.Length);

                string[] dispSplit = ContentDisposition.Split(';');
                foreach (var spl in dispSplit)
                {
                    string s = " " + spl;
                    int nameI = s.IndexOf(cName);
                    if (nameI != -1)
                    {
                        Name = s.Substring(nameI + cName.Length).Replace("\"", "");
                        continue;
                    }

                    int filenameI = s.IndexOf(cFilename);
                    if (filenameI != -1)
                    {
                        FileName = s.Substring(filenameI + cFilename.Length).Replace("\"", "");
                        IsFile = true;
                        continue;
                    }
                }
            }
        }

        private class FormDataFragmentWrap : FormDataFragment
        {
            public FormDataFragmentWrap(byte[] fragment, Encoding encoding) :
                base(fragment, encoding)
            { }
        }

        private readonly Dictionary<string, int> _nameMap = new Dictionary<string, int>();
        private readonly List<FormDataFragment> _data = new List<FormDataFragment>();
        private readonly string _boundary;
        private readonly byte[] _body;

        internal string Boundary { get => _boundary; }

        public Encoding Encoding { get; private set; }

        public HttpFormData(byte[] body, string boundary, Encoding encoding = null)
        {
            _boundary = boundary;
            _body = body;
            Encoding = encoding == null ? Encoding.ASCII : encoding;

            SplitBody();
        }

        public HttpFormData(HttpRequest request)
        {
            _body = request.Body;
            Encoding = request.ContentEncoding;

            string[] ctSplit = request.ContentTypeString.Split(';');
            foreach (var s in ctSplit)
            {
                int i = s.IndexOf("boundary=");
                if (i == -1) continue;

                _boundary = s.Substring(i + "boundary=".Length);
            }

            SplitBody();
        }

        /*
         * This function gets the multipart/form-data request body
         * and splits it when it finds a form-data boundary.
         * The resulting parts are used to construct FormDataFragment
         * classes.
         * 
         * New lines ('\n': 10) that are part of the form-data request
         * template are preceded by a carriage return character ('\r': 13).
         */
        private void SplitBody()
        {
            if (_boundary == null || _body == null)
                return;

            // boundary bytes
            byte[] bb = Encoding.GetBytes("--" + _boundary);
            int bb_length = bb.Length;

            if (_body.Length <= bb_length)
                return;

            // body should start with boundary
            for (int i = 0; i < bb_length; i++)
                if (_body[i] != bb[i])
                    return;

            int start = bb_length + 2; // start reading after boundary line ends
            int end = bb_length + 2; // temporary end

            for (int i = start; i < _body.Length; i++)
            {
                if (_body[i] == 0) // body ends with '\0'
                    if (i == _body.Length - 1)
                        return;

                if (_body[i] != 13 || _body[i + 1] != 10)
                    continue;
                // else found \r \n

                /*
                 * This was here for some reason
                 * but it broke shit.
                 * Removed it and now it works.
                 * Works with boundary='test',
                 * returns with any other web client
                 * generated boundary.
                 * Web client boundaries start with '-',
                 * maybe because of that?
                 */
                //if (_body[i + 2] == '-' && _body[i + 4] == '-')
                //    return;

                bool found_b = true;
                for (int j = 0; j < bb_length; j++)
                    if (_body[i + 2 + j] != bb[j])
                    {
                        found_b = false;
                        break;
                        // boundary not found after \r \n, continue upper loop
                    }

                if (!found_b)
                    continue;

                end = i;

                var segment = new ArraySegment<byte>(_body, start, end - start);
                _data.Add(new FormDataFragmentWrap(segment.ToArray(), Encoding));

                start = i + bb_length + 4;
                i += bb_length + 4;
            }

            IdFragments();
        }

        private void IdFragments()
        {
            for (int i = 0; i < _data.Count; i++)
                if (_data[i].Name != null)
                    _nameMap.Add(_data[i].Name, i);
        }

        public FormDataFragment this[string name]
        {
            get
            {
                if (!_nameMap.ContainsKey(name))
                    return null;

                return _data[_nameMap[name]];
            }
        }

        public FormDataFragment this[int index]
        {
            get => _data[index];
        }

        public string[] Keys
        {
            get
            {
                string[] names = new string[_nameMap.Keys.Count];
                _nameMap.Keys.CopyTo(names, 0);
                return names;
            }
        }

        public int KeyCount
        {
            get => _data.Count;
        }
    }
}
