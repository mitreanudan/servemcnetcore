﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ServeMCNetCore.HttpUtils
{
    public class HttpServer
    {
        private readonly HttpListener _listener;
        private readonly string _url;
        private bool _shutdown = false;

        public int Port { get; private set; }
        public string Host { get; private set; }
        public int MaxConnections { get; private set; }
        public int ActiveConnections { get; private set; }

        private List<string> connIds = new List<string>();
        
        public HttpServer(int port = 8080, string host = "http://localhost", int maxConn = 10)
        {
            Port = port;
            Host = host;
            MaxConnections = maxConn;

            _url = Host + ":" + Port.ToString() + "/";

            _listener = new HttpListener();
            _listener.Prefixes.Add(_url);
            App.Session.Log("Initialized Http.");
        }

        public void Start()
        {
            StartAsync().Wait();
        }

        public async Task StartAsync()
        {
            App.Session.Log("Starting Http server...");
            _listener.Start();
            App.Session.Log("Http server started.");

            while (!_shutdown)
            {
                HttpListenerContext context = await _listener.GetContextAsync();

                //old
                //HttpListenerResponse response = await HttpRouting.HandleRoute(context.Request, context.Response);

                _ = Task.Run(async () =>
                {
                    await HttpRouting.HandleRoute(context);
                });

                //response.Close();
            }
        }

        public void Shutdown()
        {
            _shutdown = true;
        }
    }
}
