﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ServeMCNetCore
{
    // ascii only
    public class Logger : IDisposable
    {
        public string LogFileName { get; private set; } = "logs/log";
        public int LogFileSize { get; private set; } = 1024 * 1024 * 10;
        public int LogFiles { get; private set; } = 5;

        private string _currentFile;
        private int _currentFileIndex = 1;
        private long _currentSize;

        private StreamWriter _stream;

        public Logger(string logFileName, int maxSize = 1024 * 1024 * 10, int maxFileCount = 10)
        {
            // '/' works on Windows too
            logFileName = logFileName.Replace('\\', '/');

            if (!string.IsNullOrEmpty(logFileName))
                LogFileName = logFileName;
            LogFiles = maxFileCount;
            LogFileSize = maxSize;

            // create all directories in case they don't exist
            int dirs = 0;
            string[] logPath = LogFileName.Split('/');
            if (logPath != null)
                dirs = logPath.Length - 1;
            if (dirs > 0)
            {
                int fileNameLength = logPath[logPath.Length - 1].Length;
                string ensureCreatedDirs = logFileName.Substring(0, LogFileName.Length - fileNameLength);
                Directory.CreateDirectory(ensureCreatedDirs);
            }

            // check for file to write to
            bool determinedFile = false;
            for (int i = 1; i <= LogFiles; i++)
            {
                string filename = LogFileName + i.ToString() + ".log";
                if (FileUtils.Utilities.FileExists(filename))
                {
                    long size = FileUtils.Utilities.GetFileSize(filename);
                    if (size >= LogFileSize)
                        continue;
                    else
                    {
                        _currentFile = filename;
                        _currentFileIndex = i;
                        _currentSize = size;
                        _stream = File.AppendText(filename);
                        determinedFile = true;
                        break;
                    }
                }
                else
                {
                    _currentFile = filename;
                    _currentFileIndex = i;
                    _currentSize = 0;
                    _stream = File.CreateText(filename);
                    determinedFile = true;
                    break;
                }
            }

            // all log files full, start again from 1
            if (!determinedFile)
            {
                _currentFile = LogFileName + "1.log";
                _currentFileIndex = 1;
                _currentSize = FileUtils.Utilities.GetFileSize(_currentFile);
                if (_currentSize > LogFileSize)
                    _stream = File.CreateText(_currentFile);
                determinedFile = true;
            }

            _stream.WriteLine("============== NEW SESSION ==============");
            _stream.WriteLine(DateTime.Now.ToString("yy-MM-dd HH:mm:ss:ffff"));
        }

        public void Log(string line)
        {
            string time = DateTime.Now.ToString("yy-MM-dd HH:mm:ss:ffff");
            line = time + " " + line;

            // 1 char = 1 byte
            _currentSize += line.Length;

            _stream.WriteLine(line);
            _stream.Flush();

            if (_currentSize >= LogFileSize)
                NextFile();
        }

        private void NextFile()
        {
            _currentFileIndex++;
            _currentFile = LogFileName + _currentFileIndex.ToString() + ".log";

            _stream.Dispose();

            if (FileUtils.Utilities.FileExists(_currentFile))
            {
                long size = FileUtils.Utilities.GetFileSize(_currentFile);
                if (size >= LogFileSize)
                {
                    _currentSize = 0;
                    _stream = File.CreateText(_currentFile);
                }
                else
                {
                    _currentSize = size;
                    _stream = File.AppendText(_currentFile);
                }
            }
            else
            {
                _currentSize = 0;
                _stream = File.CreateText(_currentFile);
            }
        }

        public void Dispose()
        {
            if (_stream != null)
                _stream.Dispose();
        }
    }
}
