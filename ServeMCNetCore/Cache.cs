﻿using System;
using System.Collections.Concurrent;
using System.Text;

namespace ServeMCNetCore
{
    public class FastCache<T>
    {
        private ConcurrentDictionary<string, T> _cache = new ConcurrentDictionary<string, T>();

        public FastCache() { }

        public T Get(string Key)
        {
            if (_cache.TryGetValue(Key, out T value))
                return value;
            return default(T);
        }

        public void Set(string Key, T Value)
        {
            _cache.AddOrUpdate(Key, Value, (arg1, arg2) => Value);
        }

        public T this[string Key]
        {
            get => Get(Key);
            set => Set(Key, value);
        }
    }

    public class Cache : FastCache<object> { }
}
