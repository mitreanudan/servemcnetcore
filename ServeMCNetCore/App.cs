﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Win32;

namespace ServeMCNetCore
{
    /*
     * TODO:
     * Redo console printing, colors persist between different
     * calls (log, debug, warning etc.) in multithreaded scenarios.
     */
    public sealed class App
    {
        public static App Session = new App();
        public Cache GlobalCache { get; private set; } = new Cache();
        public FastCache<FileUtils.ConfigFile> ConfigCache { get; private set; } = new FastCache<FileUtils.ConfigFile>();
        public bool JavaFound { get; private set; } = false;

        public ServeMCNetCoreEngine.ServerLibrary ServerLibrary { get; private set; }

        private HttpUtils.HttpServer _http;
        private Logger _logger;
        private bool _shutdown = false;

        public bool _ISDEBUG = false;

        public App()
        {
            FileUtils.DebugProxy.LogFunction = Debug;
            ServeMCNetCoreEngine.DebugProxy.LogFunction = Debug;
            ServeMCNetCoreEngine.DebugProxy.LogWarningFunction = LogWarning;
            ServeMCNetCoreEngine.DebugProxy.ReportExceptionFunction = ReportException;

            FileUtils.Utilities.ResourceManager = Properties.Resources.ResourceManager;
        }

        public void Start()
        {
            Start("servemc.cfg");
        }

        public void Start(string configFile = "servemc.cfg")
        {
            Console.WriteLine("Initializing ServeMC...");

            LoadConfiguration();

            #region DEBUG
            if (ConfigCache["servemc"]["logDebug"] == "true")
                _ISDEBUG = true;

#if DEBUG
            _ISDEBUG = true;
#endif

            if (_ISDEBUG)
                Debug("Debug constant defined.");
            #endregion

            // Please make an option to stop writing to disk debug logs
            // Http method logging takes up a lot of space but i want to see it too
            //_logger = new Logger("logs/log");

            if (!CheckJava())
            {
                LogWarning("WARNING! Java Runtime Environment not found. May not be able to run server executables.");
            }

            ServerLibrary = new ServeMCNetCoreEngine.ServerLibrary(ConfigCache["servemc"]["serverLibrary"]);
            HttpUtils.HttpRouting.LogRoutes = ConfigCache["servemc"]["logRoutes"] == "true";

            var serverTask = StartHttp();

            while (!serverTask.IsCompleted && !_shutdown)
            {
                // command parsing goes here
                string cmd = Console.ReadLine();
                Log(cmd);
            }
        }

        public void Log(string text, bool writeLog = true)
        {
            // is null checking fast enough?
            _logger?.Log(text);
            Console.WriteLine(text);
        }

        public void Log(string format, params object[] args)
        {
            string write = String.Format(format, args);
            Log(write);
        }

        public void LogWarning(string text, params object[]? args)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(string.Format(text, args));
            Console.ResetColor();
        }

        public void Debug(string text)
        {
            if (!_ISDEBUG) return;

            text = "DEBUG: " + text;
            _logger?.Log(text);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public void Debug(string text, params object[] args)
        {
            if (!_ISDEBUG) return;

            text = string.Format("DEBUG: " + text, args);
            _logger?.Log(text);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public void ReportException(Exception ex)
        {
            string site = ex.TargetSite == null ? "???" : ex.TargetSite.Name;
            string stack = string.IsNullOrEmpty(ex.StackTrace) ? "???" : ex.StackTrace;
            string message = string.IsNullOrEmpty(ex.Message) ? "Unknown exception" : ex.Message;

            string output = string.Format(
                "ERROR: {0}\nMethod: {1}\nat {2}\nMessage: {3}",
                (string)ex.Data["err"],
                site, stack, message);

            _logger?.Log(output);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(output);
            Console.ResetColor();
        }

        private void LoadConfiguration()
        {
            Log("Loading main configuration...");

            string configsProp = FileUtils.Utilities.ReadAssemblyResourceString("configs");
            string[] configs = configsProp.Split(',');

            foreach (var config in configs)
            {
                var configFile = new FileUtils.ConfigFile(config + ".cfg");

                if (!configFile.IsOpen)
                    configFile.Create();

                // reading default config for key collection integrity checking
                var configSchema = new FileUtils.ConfigFile();
                configSchema.ReadText(FileUtils.Utilities.ReadAssemblyResourceFileText(config));

                var configFileKeys = configFile.AllKeys();

                bool rewrite = false;

                // if keys from disk are missing, rewrite them with their default value
                foreach (var key in configSchema.AllKeys())
                {
                    if (!configFileKeys.Contains(key))
                    {
                        rewrite = true;
                        configFile[key] = configSchema[key];
                    }
                }

                if (rewrite)
                    configFile.Write();

                // access globally
                ConfigCache[config] = configFile;
            }
        }

        private Task StartHttp()
        {
            _http = new HttpUtils.HttpServer();
            return _http.StartAsync();
        }

        public bool CheckJava()
        {
            if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.Windows))
            {
                RegistryKey rk = Registry.LocalMachine;
                var sub = rk.OpenSubKey("SOFTWARE\\JavaSoft\\Java Runtime Environment");

                string version = (string)sub.GetValue("CurrentVersion");

                if (string.IsNullOrEmpty(version))
                    return false;
            }

            Process _p = new Process();
            _p.StartInfo = new ProcessStartInfo
            {
                FileName = "java",
                Arguments = "-version",
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            try
            {
                _p.Start();
                string stdout = _p.StandardOutput.ReadToEnd();
                string stderr = _p.StandardError.ReadToEnd();

                _p.WaitForExit();

                if (!stdout.StartsWith("java version") && !stderr.StartsWith("java version"))
                    return false;
            }
            catch { return false; }

            JavaFound = true;
            return true;
        }
    }
}
