﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeMCNetCore.HttpUtils
{
    /// <summary>
    /// DEPRECATED! Use <see cref="ServeMCNetCore.HttpUtils.Controllers.Controller.WebStatic(HttpRequest)"/> and <see cref="ServeMCNetCore.HttpUtils.Controllers.Controller.Content(string)"/> instead.
    /// </summary>
    public class HtmlPage : HttpUtils.HttpResponse
    {
        public HtmlPage(string content) : base()
        {
            ContentType = Type.Html;
            Content = content;
        }

        public static HtmlPage From(string file)
        {
            file = file.EndsWith(".html") ? file.Substring(0, file.Length - 5) : file;
            string html = FileUtils.Utilities.ReadAssemblyResourceFileText(file);

            return new HtmlPage(html);
        }

        public static HttpResponse Static(string file)
        {
            file = file.EndsWith(".html") ? file : file + ".html";
            try
            {
                string fileContent = FileUtils.Utilities.GetFileText(file);

                return new HtmlPage(fileContent);
            }
            catch
            {
                return Controllers.MainController.E404(HttpRequest.Empty);
            }
        }
    }
}
