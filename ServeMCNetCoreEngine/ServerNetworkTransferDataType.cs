﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ServeMCNetCoreEngine
{
    public partial class ServerNetworkTransfer
    {
        public static class DataType
        {
            public class VarInt
            {
                public enum Endianness { BigEndian, LittleEndian };

                private int intValue;
                private byte[] byteValue; // will be stored little-endian!!!
                private Endianness endian;

                public int Value
                {
                    get => intValue;
                }

                public ReadOnlyCollection<byte> Bytes
                {
                    get
                    {
                        if (endian == Endianness.BigEndian)
                        {
                            byte[] temp = new byte[byteValue.Length];
                            byteValue.CopyTo(temp, 0);
                            Array.Reverse(temp);
                            return new ReadOnlyCollection<byte>(temp);
                        }

                        return new ReadOnlyCollection<byte>(byteValue);
                    }
                }

                public VarInt(int value, Endianness endianness = Endianness.LittleEndian)
                {
                    endian = endianness;
                    DecodeInt(value);
                }

                public VarInt(byte[] value, Endianness endianness = Endianness.LittleEndian)
                {
                    endian = endianness;

                    // redo this, makes no sense when reading buffer
                    if (endianness == Endianness.BigEndian)
                        Array.Reverse(byteValue);

                    DecodeBytes(value);
                }

                private void DecodeInt(int integer)
                {
                    int i = integer;
                    var bytes = new List<byte>();

                    do
                    {
                        byte temp = (byte)(i & 0x7F);
                        i >>= 7;
                        if (i != 0)
                            temp |= 0x80;
                        bytes.Add(temp);
                    } while (i != 0);

                    byteValue = bytes.ToArray();
                    intValue = integer;
                }

                private void DecodeBytes(byte[] byteArr)
                {
                    if (byteArr == null)
                        throw new ArgumentNullException("byteArr");

                    var value = 0;

                    // should start at one, but messes with size++ from inside the loop, increment later.
                    var size = 0;

                    int i;
                    for (i = 0; i < byteArr.Length && (byteArr[i] & 0x80) == 0x80; i++)
                        value |= (byteArr[i] & 0x7F) << (size++ * 7);

                    if (size > 5)
                        DebugProxy.LogWarning("WARNING Decoded VarInt longer than 5 bytes. These are not supposed to exist.");

                    intValue = value | ((byteArr[i] & 0x7F) << (size * 7));

                    size++; // increment here
                    byteValue = new byte[size];
                    Array.Copy(byteArr, byteValue, size);
                }
            }

            public abstract class BasicType<T>
            {
                private T value;

                public T Value { get => GetValue(); }
                public ReadOnlyCollection<byte> Bytes { get => GetBytes(); }

                public BasicType(T value)
                {
                    this.value = value;
                }

                protected virtual T GetValue()
                {
                    return value;
                }
                protected abstract ReadOnlyCollection<byte> GetBytes();
            }

            public class Int : BasicType<int>
            {
                public Int(int value) : base(value) { }

                protected override ReadOnlyCollection<byte> GetBytes()
                {
                    return new ReadOnlyCollection<byte>(BitConverter.GetBytes(GetValue()));
                }
            }

            public class Short : BasicType<short>
            {
                public Short(short value) : base(value) { }

                protected override ReadOnlyCollection<byte> GetBytes()
                {
                    return new ReadOnlyCollection<byte>(BitConverter.GetBytes(GetValue()));
                }
            }

            public class String : BasicType<string>
            {
                public String(string value) : base(value) { }

                protected override ReadOnlyCollection<byte> GetBytes()
                {
                    return new ReadOnlyCollection<byte>(Encoding.ASCII.GetBytes(GetValue()));
                }
            }
        }
    }
}
