﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ServeMCNetCoreEngine
{
    public partial class ServerNetworkTransfer
    {
        public interface ITransferType
        {
            public ReadOnlyCollection<byte> GetByteSequence();
        }

        private class TransferVarInt : DataType.VarInt, ITransferType
        {
            public TransferVarInt(int value) : base(value) { }
            public TransferVarInt(byte[] value) : base(value) { }

            public ReadOnlyCollection<byte> GetByteSequence()
            {
                return Bytes; // from base
            }
        }

        private class TransferShort : DataType.Short, ITransferType
        {
            public TransferShort(short value) : base(value) { }

            public ReadOnlyCollection<byte> GetByteSequence()
            {
                return Bytes; // from base
            }
        }

        /// <summary>
        /// Implements representation of a string when used for transferring text.
        /// Within this representation the length of the string in the form of a VarInt
        /// must precede the actual content. For a <see cref="TransferString"/> without
        /// the length prefix, please use <see cref="TransferStringNoPrefix"/>.
        /// </summary>
        private class TransferString : DataType.String, ITransferType
        {
            public TransferString(string value) : base(value) { }

            public ReadOnlyCollection<byte> GetByteSequence()
            {
                // When being transferred by the default protocol, strings are preceded by their length.
                int length = Value.Length;
                var varInt = new TransferVarInt(length);

                var newBytes = new List<byte>();
                newBytes.AddRange(varInt.GetByteSequence());
                newBytes.AddRange(Bytes);

                return new ReadOnlyCollection<byte>(newBytes);
            }
        }

        private class TransferStringNoPrefix : DataType.String, ITransferType
        {
            public TransferStringNoPrefix(string value) : base(value) { }

            public ReadOnlyCollection<byte> GetByteSequence()
            {
                return Bytes; // from base
            }
        }
    }
}
