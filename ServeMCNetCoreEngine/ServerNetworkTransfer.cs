﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

/*
 * Server communication implementation.
 * 
 * TODO: Change to async?
 */
namespace ServeMCNetCoreEngine
{
    public partial class ServerNetworkTransfer : IDisposable
    {
        bool _disposed = false;

        TcpClient _client;
        NetworkStream _stream; // careful to dispose?

        public delegate void OnConnectedHandler();
        public delegate void OnConnectionFaultHandler(Exception reason);

        private string _host;
        private int _port;
        public bool Connected { get => _client.Connected; }
        public bool Fail { get; private set; } = true;
        public OnConnectedHandler OnConnect { private get; set; }
        public OnConnectionFaultHandler OnConnectionFault { private get; set; }

        public ServerNetworkTransfer(string host = "127.0.0.1", int port = 25565)
        {
            _host = host;
            _port = port;

            _client = new TcpClient();
        }

        ~ServerNetworkTransfer()
        {
            Dispose(false);
        }

        public void Connect()
        {
            DebugProxy.Log("Attempting to connect the TCP client to server...");
            try
            {
                _client.Connect(_host, _port);
            }
            catch
            {
                DebugProxy.Log("TCP client connection unsuccessful.");
            }

            if (_client.Connected)
            {
                DebugProxy.Log("TCP client connected.");
                Fail = false;
                _stream = _client.GetStream();
                OnConnect?.Invoke();
            }
            else
                DebugProxy.LogWarning("TCP client connection unsuccessful. Some features may be unusable.");
        }

        public Task ConnectAsync()
        {
            DebugProxy.Log("Attempting to connect the TCP client to server...");

            int tryCount = 0;
            return Task.Run(() =>
            {
                Exception lastException = new Exception();
                do
                {
                    try
                    {
                        _client.Connect(_host, _port);
                    }
                    catch (Exception ex)
                    {
                        lastException = ex;
                        DebugProxy.Log("TCP client connection unsuccessful. Retrying...");
                    }

                    if (!_client.Connected)
                        Thread.Sleep(3000);

                } while (!_client.Connected && tryCount++ < 10);

                if (_client.Connected)
                {
                    DebugProxy.Log("TCP client connected.");
                    Fail = false;
                    _stream = _client.GetStream();
                    OnConnect?.Invoke();
                }
                else
                {
                    DebugProxy.Log("TCP client connection unsuccessful.");
                    OnConnectionFault?.Invoke(lastException);
                }
            });
        }

        public void Send(Message packet)
        {
            if (!_client.Connected)
                throw new Exception("TCP Client not connected to server. Cannot send packet.");
            _stream.Write(packet.GetData());
        }

        public byte[] ReceiveRaw(int bufferSize = Int16.MaxValue)
        {
            byte[] buffer = new byte[bufferSize];
            int recCount = _stream.Read(buffer);

            if (recCount > bufferSize)
                throw new OverflowException("ServerNetworkTransfer: Received more bytes than buffer size allows.");

            byte[] rec = new byte[recCount];
            Array.Copy(buffer, rec, recCount);

            return rec;
        }

        public ResponseMessage Receive(int bufferSize = Int16.MaxValue)
        {
            return new ResponseMessage(ReceiveRaw(bufferSize));
        }

        public ResponseMessage SendAndReceive(Message packet, int bufferSize = Int16.MaxValue)
        {
            Send(packet);
            return Receive(bufferSize);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _stream?.Dispose();
                _client?.Close();
            }

            _client?.Dispose();
            OnConnect = null;

            _disposed = true;
        }
    }
}
