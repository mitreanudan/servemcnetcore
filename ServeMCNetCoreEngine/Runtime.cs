﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace ServeMCNetCoreEngine
{
    public sealed class Runtime : IDisposable
    {
        public bool Disposed { get; private set; }

        public delegate void OutputHandler(object senderProcess, DataReceivedEventArgs outData);
        public delegate void ProcessExitHandler();

        private Process _process = new Process();
        private string startOptions;
        private string workingDirectory;
        public bool Running { get; private set; }
        public bool Stopped { get; private set; } = false;
        public DateTime StartTime { get; private set; }

        public string StartFile { get; private set; }
        public string StartOptions {
            get => startOptions;
            set
            {
                startOptions = value;
                if (!string.IsNullOrEmpty(startOptions))
                    _process.StartInfo.Arguments = startOptions;
            }
        }
        public string WorkingDirectory
        {
            get => workingDirectory;
            set
            {
                workingDirectory = value;
                _process.StartInfo.WorkingDirectory = workingDirectory;
            }
        }
        public OutputHandler HandleOutput { private get; set; }
        public OutputHandler HandleError { private get; set; }
        public ProcessExitHandler HandleExit { private get; set; }
        public ProcessPriorityClass Priority { private get; set; } = ProcessPriorityClass.Normal;

        public Runtime(string startFileName, string startOptions = null, string workingDirectory = null)
        {
            _process = new Process();
            _process.StartInfo = new ProcessStartInfo
            {
                FileName = startFileName,
                Arguments = startOptions ?? string.Empty,
                WorkingDirectory = workingDirectory ?? string.Empty,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            _process.EnableRaisingEvents = true;
            _process.OutputDataReceived += HandleOutputIntern;
            _process.ErrorDataReceived += HandleErrorIntern;
            _process.Exited += ProcessExit;
        }

        ~Runtime()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (Disposed)
                return;

            if (disposing)
            {
                _process?.Dispose();
            }

            HandleOutput = null;
            HandleExit = null;
            HandleError = null;

            Disposed = true;
        }

        public void Start()
        {
            _process.Start();
            _process.PriorityClass = Priority;
            _process.BeginOutputReadLine();
            _process.BeginErrorReadLine();
            StartTime = DateTime.Now;
            Running = true;
        }

        public void Kill()
        {
            if (!Running)
                return;

            _process.Kill();
            Running = false;
        }

        public void SendInput(string line)
        {
            _process.StandardInput.WriteLine(line);
        }

        public void FreeProcess()
        {
            if (Running)
                throw new Exception("Cannot dispose process instance while it is running.");

            _process?.Dispose();
        }

        private void HandleOutputIntern(object senderProcess, DataReceivedEventArgs outData)
        {
            HandleOutput?.Invoke(senderProcess, outData);
        }

        private void HandleErrorIntern(object senderProcess, DataReceivedEventArgs outData)
        {
            HandleError?.Invoke(senderProcess, outData);
        }

        private void ProcessExit(object sender, EventArgs e)
        {
            Running = false;
            Stopped = true;

            HandleExit?.Invoke();
        }
    }
}
