﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeMCNetCoreEngine
{
    public partial class ServerNetworkTransfer
    {
        public class MinecraftMessage : Message
        {
            private int id;

            public MinecraftMessage() : this(0) { }

            public MinecraftMessage(int packetId) : base()
            {
                id = packetId;
            }

            public override byte[] GetData()
            {
                byte[] baseData = base.GetData();
                int packetId = id >= 0 ? id : 0;
                var packetIdVarInt = new TransferVarInt(packetId);
                int packetIdLength = packetIdVarInt.Bytes.Count;

                int totalLength = packetIdLength + baseData.Length;

                var temp = new Message();
                temp.AddField(totalLength);
                temp.AddField(packetIdVarInt);
                temp.AddFieldRaw(baseData);

                return temp.GetData();
            }

            public static MinecraftMessage CreateHandshakeMessage(int protocol = 578, string host = "localhost", short port = 25565)
            {
                // handshake
                var msg = new MinecraftMessage(packetId: 0);
                msg.AddField(protocol);
                msg.AddField(host, true);
                msg.AddField(port);
                msg.AddField(1); // next state

                return msg;
            }

            public static MinecraftMessage CreatePingMessage()
            {
                return new MinecraftMessage(packetId: 0);
            }
        }
    }
}
