﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections.ObjectModel;
using System.Text.Json;
using FileUtils = ServeMCNetCore.FileUtils;

namespace ServeMCNetCoreEngine
{
    // Just searches or creates a directory for server packages
    // Wraps multiple ServerPackages and runs them
    public class ServerLibrary
    {
        public string Path { get; private set; }

        private readonly List<ServerPackage> _servers = new List<ServerPackage>();

        public ReadOnlyCollection<ServerPackage> Servers { get; private set; }
        public bool Empty { get => Servers == null; }

        public ServerPackage ActiveInstance { get; private set; }
        public bool ActiveInstanceRunning { get => ActiveInstance == null ? false : ActiveInstance.Running; }

        public ServerPackage this[string id]
        {
            get
            {
                foreach (var server in _servers)
                    if (server.Id == id)
                        return server;
                return null;
            }
        }

        public ServerLibrary(string path)
        {
            FileUtils.Utilities.EnsureDirectory(path);
            Path = path;

            string[] serverPaths = FileUtils.Utilities.SubdirectoriesOf(path);
            if (serverPaths.Length == 0)
                return;

            foreach (var s in serverPaths)
            {
                string jsonConfigPath = System.IO.Path.Combine(s, "config.json");
                string jsonConfig = FileUtils.Utilities.GetFileText(jsonConfigPath);
                ServerPackageConfig config = JsonSerializer.Deserialize<ServerPackageConfig>(jsonConfig);
                if (config == null)
                    continue;

                var package = new ServerPackage(config);
                package.ServerDirectory = s;
                _servers.Add(package);
            }

            Servers = _servers.AsReadOnly() ?? new ReadOnlyCollection<ServerPackage>(new List<ServerPackage>());
        }

        public string CreateServer(string jsonServerConfig, byte[] jar)
        {
            try
            {
                var serverConfig = JsonSerializer.Deserialize<ServerPackageConfig>(jsonServerConfig);
                DebugProxy.Log(serverConfig.LastRunTick.ToString());
                serverConfig.Id = Guid.NewGuid().ToString();

                string serverPath = System.IO.Path.Combine(Path, serverConfig.Id);
                string serverJar = System.IO.Path.Combine(serverPath, "server.jar");
                string serverJson = System.IO.Path.Combine(serverPath, "config.json");

                var newServer = new ServerPackageConfig(serverConfig);
                newServer.ServerJar = "server.jar";
                newServer.FirstStart = true;
                string newServerConfig = JsonSerializer.Serialize(newServer);

                FileUtils.Utilities.EnsureDirectory(serverPath);
                FileUtils.Utilities.CreateFileFromBytes(serverJar, jar);
                FileUtils.Utilities.CreateFileText(serverJson, newServerConfig);

                _servers.Add(new ServerPackage(newServer));
                Servers = _servers.AsReadOnly();

                return newServer.Id;
            }
            catch (Exception ex)
            {
                ex.Data["err"] = "Error while creating server.";
                DebugProxy.ReportException(ex);
                return null;
            }
        }

        public string GetServerJson(string id)
        {
            var server = this[id];
            if (server == null)
                return null;
            return JsonSerializer.Serialize(this[id].GetConfig());
        }

        public bool StartServer(string id)
        {
            if (this[id] == null)
            {
                DebugProxy.LogWarning("WARNING Could not start server with ID {0} because it does not exist.", id);
                return false;
            }

            if (this[id].Running)
            {
                DebugProxy.LogWarning("WARNING Server instance already running, please stop it before starting another server.");
                return false;
            }

            ActiveInstance = this[id];
            ActiveInstance.Start();

            return true;
        }
    }
}
