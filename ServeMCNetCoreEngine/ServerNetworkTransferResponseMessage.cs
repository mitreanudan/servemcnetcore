﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeMCNetCoreEngine
{
    public partial class ServerNetworkTransfer
    {
        public class ResponseMessage : Message
        {
            private int _readPos = 0;
            public int ReadPosition
            {
                get => _readPos;
                set
                {
                    if (value > DataLength)
                        throw new ArgumentOutOfRangeException("Cannot set ReadPosition to a value larger than the message length.");
                    if (value < 0)
                        throw new ArgumentOutOfRangeException("Cannot set ReadPostion to a negative value");

                    _readPos = value;
                }
            }

            public ResponseMessage(byte[] data) : base()
            {
                AddFieldRaw(data);
            }

            public virtual DataType.VarInt ReadNextVarInt()
            {
                if (_readPos + 1 > DataLength)
                    throw new Exception("End of buffer.");

                var byteSegment = new ArraySegment<byte>(Data, _readPos, DataLength - _readPos);
                var varInt = new DataType.VarInt(byteSegment.ToArray());

                _readPos += varInt.Bytes.Count;

                return varInt;
            }

            public virtual DataType.Short ReadNextShort()
            {
                if (_readPos + 2 > DataLength)
                    throw new Exception("End of buffer.");

                var byteSegment = new ArraySegment<byte>(Data, _readPos, 2);

                _readPos += 2;

                return new DataType.Short(BitConverter.ToInt16(byteSegment.ToArray()));
            }

            public virtual DataType.Int ReadNextInt()
            {
                if (_readPos + 4 > DataLength)
                    throw new Exception("End of buffer.");

                var byteSegment = new ArraySegment<byte>(Data, _readPos, 4);

                _readPos += 4;

                return new DataType.Int(BitConverter.ToInt32(byteSegment.ToArray()));
            }

            /// <summary>
            /// Gets the string length in VarInt and then reads the next
            /// characters according to the VarInt's value. Use <see cref="ReadNextStringNoLength(int)"/>
            /// to skip reading a VarInt and then reading the string.
            /// </summary>
            /// <returns></returns>
            public virtual DataType.String ReadNextString()
            {
                if (_readPos + 1 > DataLength)
                    throw new Exception("End of buffer.");

                int length = ReadNextVarInt().Value;

                if (_readPos + length > DataLength)
                    throw new Exception("End of buffer.");

                var byteSegment = new ArraySegment<byte>(Data, _readPos, length);

                _readPos += length;

                return new DataType.String(Encoding.ASCII.GetString(byteSegment.ToArray()));
            }

            public virtual DataType.String ReadNextStringNoLength(int length)
            {
                if (_readPos + length > DataLength)
                    throw new Exception("End of buffer.");

                var byteSegment = new ArraySegment<byte>(Data, _readPos, length);

                _readPos += length;

                return new DataType.String(Encoding.ASCII.GetString(byteSegment.ToArray()));
            }
        }
    }
}
