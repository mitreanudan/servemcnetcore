﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeMCNetCoreEngine
{
    public partial class ServerNetworkTransfer
    {
        public class Message
        {
            private List<byte> _bytes;
            protected byte[] Data { get => _bytes.ToArray(); }
            protected int DataLength { get => _bytes.Count; }

            public Message()
            {
                _bytes = new List<byte>();
            }

            internal void AddFieldRaw(byte[] data)
            {
                _bytes.AddRange(data);
            }

            public void AddField(string data, bool addLengthPrefix = true)
            {
                ITransferType content;

                if (addLengthPrefix)
                    content = new TransferString(data);
                else
                    content = new TransferStringNoPrefix(data);

                AddField(content);
            }

            public void AddField(int data) => AddField(new TransferVarInt(data));
            public void AddField(short data) => AddField(new TransferShort(data));

            public void AddField(ITransferType data)
            {
                AddFieldData(data);
            }

            private void AddFieldData(ITransferType data)
            {
                _bytes.AddRange(data.GetByteSequence());
            }

            public virtual byte[] GetData()
            {
                return _bytes.ToArray();
            }
        }
    }
}
