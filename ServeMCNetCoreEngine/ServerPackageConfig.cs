﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ServeMCNetCoreEngine
{
    public class ServerPackageConfig
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("jar")]
        public string ServerJar { get; set; }
        [JsonPropertyName("serverType")]
        public string ServerTypeString { get; set; }
        [JsonPropertyName("javaOptions")]
        public string JavaOptions { get; set; } = "-Xms128M -Xmx512M -jar server.jar nogui";
        [JsonPropertyName("minMemory")]
        public int MinMemory { get; set; } = 128;
        [JsonPropertyName("maxMemory")]
        public int MaxMemory { get; set; } = 512;
        [JsonPropertyName("firstStart")]
        public bool FirstStart { get; set; }
        [JsonPropertyName("issueDate")]
        public string IssueDate { get; set; }
        [JsonPropertyName("lastRun")]
        public long LastRunTick { get; set; }

        public ServerPackageConfig() { }

        public ServerPackageConfig(ServerPackageConfig copy)
        {
            Id = copy.Id;
            Name = copy.Name;
            ServerJar = copy.ServerJar;
            ServerTypeString = copy.ServerTypeString;
            JavaOptions = copy.JavaOptions;
            MinMemory = copy.MinMemory;
            MaxMemory = copy.MaxMemory;
            FirstStart = copy.FirstStart;
            IssueDate = copy.IssueDate;
            LastRunTick = copy.LastRunTick;
        }

        [JsonIgnore]
        public ServerPackage.ServerProviderType ServerType
        {
            get
            {
                switch (ServerTypeString)
                {
                    case "paper":
                        return ServerPackage.ServerProviderType.Paper;
                    case "custom":
                        return ServerPackage.ServerProviderType.Custom;
                    default:
                        return ServerPackage.ServerProviderType.Vanilla;
                }
            }
            set
            {
                switch (value)
                {
                    case ServerPackage.ServerProviderType.Paper:
                        ServerTypeString = "paper";
                        break;
                    case ServerPackage.ServerProviderType.Custom:
                        ServerTypeString = "custom";
                        break;
                    default:
                        ServerTypeString = "vanilla";
                        break;
                }
            }
        }

        [JsonIgnore]
        public DateTime LastRun
        {
            get => new DateTime(LastRunTick);
            set => LastRunTick = value.Ticks;
        }
    }
}
