﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeMCNetCoreEngine
{
    public class RuntimeParameterChangeException : Exception
    {
        public string ParameterName { get; private set; }

        public RuntimeParameterChangeException() { }

        public RuntimeParameterChangeException(string parameter) :
            base(string.Format("Cannot change parameter {0} after runtime has started.", parameter))
        {
            ParameterName = parameter;
        }

        public RuntimeParameterChangeException(string parameter, params object[] parameterFormats)
            : this(string.Format(parameter, parameterFormats))
        { }
    }
}
