﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("ServeMCNetCore")]
namespace ServeMCNetCoreEngine
{
    internal static class DebugProxy
    {
        public delegate void DebugLogDelegate(string text, params object[]? format);
        public delegate void ReportExceptionDelegate(Exception ex);

        public static DebugLogDelegate LogFunction { private get; set; }
        public static ReportExceptionDelegate ReportExceptionFunction { private get; set; }
        public static DebugLogDelegate LogWarningFunction { private get; set; }

        internal static void Log(string text)
        {
            LogFunction?.Invoke(text);
        }

        internal static void Log(string text, params object[] format)
        {
            LogFunction?.Invoke(text, format);
        }

        internal static void ReportException(Exception ex)
        {
            ReportExceptionFunction?.Invoke(ex);
        }

        internal static void LogWarning(string text)
        {
            LogWarningFunction?.Invoke(text);
        }

        internal static void LogWarning(string text, params object[] format)
        {
            LogWarningFunction?.Invoke(text, format);
        }
    }
}
