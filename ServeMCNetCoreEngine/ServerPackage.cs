﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

using FileUtils = ServeMCNetCore.FileUtils;
using ConfigFile = ServeMCNetCore.FileUtils.ConfigFile;

namespace ServeMCNetCoreEngine
{
    // todo: using events, make idisposable
    public class ServerPackage
    {
        public delegate void OutputStreamHandler(string output, bool danger = false);
        public delegate void ServerExitHandler();

        public const string DefaultJavaStartOptions = "-Xms{minMem}M -Xmx{maxMem}M -jar {jar} nogui";

        public enum ServerProviderType { Vanilla, Paper, Custom };

        private Runtime _runtime;
        private ServerNetworkTransfer _networkComm;
        private string jarFile = "server.jar";
        private string serverDirectory = string.Empty;

        // in Mbytes ONLY
        private int minMemory = 128;
        private int maxMemory = 1024;

        private string stdoutStream = string.Empty;
        private string stderrStream = string.Empty;
        private string statusStream = string.Empty;
        private bool stdoutUpdate = false;
        private bool stderrUpdate = false;
        private bool statusUpdate = false;

        public string ConsoleContent { get; private set; } = string.Empty;
        public bool Stopped { get; private set; } = true;
        public bool Running { get => _runtime == null ? false : _runtime.Running; }
        private bool eula = false;
        public bool Eula
        {
            get => eula;
            private set
            {
                eula = value;
                if (!eula)
                    UpdateServerStatus("eula_fault");
            }
        }
        public ConfigFile ServerProperties { get; private set; }
        public int ServerPort { get; private set; }
        public Dictionary<string, ConfigFile> Configs { get; private set; }
        public event OutputStreamHandler OutputAction;
        public event ServerExitHandler ExitAction;
        internal string ServerDirectory
        {
            get => serverDirectory;
            set
            {
                serverDirectory = value;
                if (_runtime != null)
                    _runtime.WorkingDirectory = serverDirectory;
            }
        }
        public bool NetworkConnection { get; private set; } = false;
        public bool NetworkHandshake { get; private set; } = false;
        private string statusPing;
        // public StatusPingModel StatusPing !

        public string Id { get; private set; }
        public string Name { get; set; }
        public string JavaOptions { get; set; } = "";
        public ServerProviderType ServerType { get; set; }
        public bool HasConfig { get; private set; } = false;
        public bool FirstStart { get; private set; } = true;
        public string IssueDate { get; private set; }
        public DateTime LastRun { get; set; }

        public string ServerJar
        {
            get => jarFile;
            private set
            {
                UpdateJavaOptions("-jar {0}", jarFile, value);
                jarFile = value;
            }
        }

        // Mbytes ONLY
        public int MinMemory
        {
            get => minMemory;
            set
            {
                RemoveJavaOption("-Xms");
                UpdateJavaOptions("-Xms{0}M", minMemory, value);
                minMemory = value;
            }
        }
        public int MaxMemory
        {
            get => maxMemory;
            set
            {
                RemoveJavaOption("-Xmx");
                UpdateJavaOptions("-Xmx{0}M", maxMemory, value);
                maxMemory = value;
            }
        }

        public ServerPackage(string serverName, string serverJar, string javaOptions = null)
        {
            Id = Guid.NewGuid().ToString();
            Name = serverName;
            ServerJar = serverJar;

            string startOptions = DefaultJavaStartOptions
                .Replace("{minMem}", minMemory.ToString())
                .Replace("{maxMem}", maxMemory.ToString())
                .Replace("{jar}", ServerJar);

            JavaOptions = javaOptions ?? startOptions;
        }

        public ServerPackage(ServerPackageConfig config)
        {
            Id = config.Id;
            Name = config.Name;
            ServerJar = config.ServerJar.EndsWith(".jar") ? config.ServerJar : config.ServerJar + ".jar";
            ServerType = config.ServerType;
            JavaOptions = config.JavaOptions;
            MinMemory = config.MinMemory;
            MaxMemory = config.MaxMemory;
            IssueDate = config.IssueDate;
            LastRun = config.LastRun;

            HasConfig = true;

            if (!JavaOptions.Contains("-jar "))
                JavaOptions += string.Format(" -jar {0} nogui", ServerJar);
        }

        /// <summary>
        /// <code>this(JsonSerializer.Deserialize&lt;ServerPackageConfig&gt;(jsonConfig))</code>
        /// <para>Deprecated!!!</para>
        /// <para>Do null checking for deserialized JSON before constructing.</para>
        /// </summary>
        /// <param name="jsonConfig">Json serialization for ServerPackageConfig.</param>
        public ServerPackage(string jsonConfig) :
            this(JsonSerializer.Deserialize<ServerPackageConfig>(jsonConfig))
        {
            DebugProxy.Log("ServerPackage::ServerPackage(const char* jsonConfig) is deprecated!!!");    // not using c++ but whatever
            DebugProxy.Log("Use ServerPackage::ServerPackage(ServerPackageConfig config) instead.");    // guess that works too
        }

        public ServerPackageConfig GetConfig()
        {
            return new ServerPackageConfig
            {
                Id = Id,
                Name = Name,
                ServerJar = ServerJar,
                ServerType = ServerType,
                JavaOptions = JavaOptions,
                MinMemory = MinMemory,
                MaxMemory = MaxMemory,
                FirstStart = FirstStart,
                IssueDate = IssueDate,
                LastRun = LastRun
            };
        }

        private void RewriteConfig()
        {
            string json = JsonSerializer.Serialize(GetConfig());
            string jsonPath = System.IO.Path.Combine(ServerDirectory, "config.json");

            FileUtils.Utilities.CreateFileText(jsonPath, json);
        }

        private void CreateRuntime(string startOptions = null, string workingDirectory = null)
        {
            _runtime = new Runtime("java", startOptions, workingDirectory);
            _runtime.HandleOutput = RuntimeOutputStreamHandler;
            _runtime.HandleError = RuntimeErrorStreamHandler;
            _runtime.HandleExit = RuntimeExitHandler;
        }

        private void UpdateJavaOptions(string paramFormat, object oldValue, object newValue)
        {
            // ("-Xms{0}M", 1024, 2048) => find and change -Xms1024M to -Xms2048M
            string oldParam = string.Format(paramFormat, oldValue);
            string newParam = string.Format(paramFormat, newValue);

            if (JavaOptions.Contains(oldParam))
                JavaOptions.Replace(oldParam, newParam);
            else
                // or just add it if not present
                JavaOptions = newParam + " " + JavaOptions;
        }

        private void RemoveJavaOption(string optionKey)
        {
            // ("-Xms") => remove -Xms{int}M
            if (!JavaOptions.Contains(optionKey))
                return;

            int optionStart = JavaOptions.IndexOf(optionKey);
            if (optionStart == -1)
                return;

            int optionEnd = JavaOptions.IndexOf(' ', optionStart);

            string half1 = JavaOptions.Substring(0, optionStart).Replace("  ", " ");
            string half2 = JavaOptions.Substring(optionEnd).Replace("  ", " ");
            JavaOptions = (half1 + half2).Replace("  ", " ");
        }

        private void UpdateServerStatus(string key)
        {
            statusUpdate = true;

            if (statusStream.Length == 0)
            {
                statusStream = key;
                return;
            }

            statusStream += ',' + key;
        }

        private void RuntimeOutputStreamHandler(object senderProcess, System.Diagnostics.DataReceivedEventArgs output)
        {
            if (string.IsNullOrEmpty(output.Data))
                return;

            stdoutStream += output.Data + '\n';
            ConsoleContent += output.Data + "\n\r";
            stdoutUpdate = true;
            OutputAction?.Invoke(output.Data);
        }

        private void RuntimeErrorStreamHandler(object senderProcess, System.Diagnostics.DataReceivedEventArgs output)
        {
            if (string.IsNullOrEmpty(output.Data))
                return;

            stderrStream += output.Data + '\n';
            ConsoleContent += "!{servemc:warn}!" + output.Data + "\n\r";
            stderrUpdate = true;
            OutputAction?.Invoke(output.Data, danger: true);
        }

        private void RuntimeExitHandler()
        {
            Stopped = true;
            _networkComm?.Dispose();

            ExitAction?.Invoke();
        }

        public void Start()
        {
            Stopped = false;
            FirstStart = false;
            ConsoleContent = string.Empty;
            LastRun = DateTime.Now;
            RewriteConfig();

            if (_runtime != null && !_runtime.Disposed)
                _runtime.Dispose();

            CreateRuntime(JavaOptions, ServerDirectory);

            _runtime.Start();

            string eulatxtPath = System.IO.Path.Combine(ServerDirectory, "eula.txt");
            if (FileUtils.Utilities.FileExists(eulatxtPath))
            {
                var eulaConfig = new ConfigFile(eulatxtPath);
                string eulaVal = eulaConfig["eula"];
                if (eulaVal != null && eulaVal.ToLower() == "true")
                    Eula = true;
                else
                    Eula = false;
            }
            else
                Eula = false;

            if (!Eula)
            {
                DebugProxy.Log("Found server eula value to be false.");
                return;
            }

            string serverPropPath = System.IO.Path.Combine(ServerDirectory, "server.properties");
            if (FileUtils.Utilities.FileExists(serverPropPath))
            {
                var serverProp = new ConfigFile(serverPropPath);
                if (!Int32.TryParse(serverProp["server-port"], out int serverPort))
                    ServerPort = 25565;
                else
                    ServerPort = serverPort;
            }
            else
                ServerPort = 25565;

            // todo: initiate server network transfer

            _networkComm = new ServerNetworkTransfer("127.0.0.1", ServerPort);
            _networkComm.OnConnect = () =>
            {
                NetworkConnection = true;
                PerformHandshake();
                PerformStatusPing();
                DebugProxy.Log("{0}", statusPing);
            };
            _networkComm.OnConnectionFault = (reason) =>
            {
                NetworkConnection = false;
                UpdateServerStatus("network_contact_fault");
                DebugProxy.ReportException(reason);
                DebugProxy.LogWarning("Could not establish network connection to the server. Some features may be unusable.");
            };
            _networkComm.ConnectAsync();
        }

        internal void Start(OutputStreamHandler outStreamAction)
        {
            OutputAction += outStreamAction;
            Start();
        }

        /// <summary>
        /// To be used inside a while loop.
        /// <code>while (serverPackage.GetOutput(out output, out error, out status)) do_stuff(output, error, status);</code>
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        public bool GetOutput(out string output, out string error, out string status)
        {
            string prepOut = null;
            string prepErr = null;
            string prepSts = null;

            // wait until there's something to show
            while (!stdoutUpdate && !stderrUpdate)
                if (!_runtime.Running)
                {
                    output = error = status = null;
                    Stopped = true;
                    ConsoleContent = string.Empty;
                    return false;
                }

            if (stdoutUpdate)
                prepOut = stdoutStream;

            if (stderrUpdate)
                prepErr = stderrStream;

            if (statusUpdate)
                prepSts = statusStream;

            stdoutStream = stderrStream = statusStream = string.Empty;
            stdoutUpdate = stderrUpdate = statusUpdate = false;

            output = prepOut;
            error = prepErr;
            status = prepSts;
            return true;
        }

        public void SendInput(string input)
        {
            _runtime.SendInput(input);
        }

        private void PerformHandshake()
        {
            var handshake = ServerNetworkTransfer.MinecraftMessage.CreateHandshakeMessage(578, "127.0.0.1", (short)ServerPort);
            _networkComm.Send(handshake);
            NetworkHandshake = true;
        }

        private void PerformStatusPing()
        {
            if (!NetworkHandshake)
                throw new Exception("Cannot send status ping request without having completed handshake.");

            _networkComm.Send(ServerNetworkTransfer.MinecraftMessage.CreatePingMessage());
            var response = _networkComm.Receive();
            int responseLength = response.ReadNextVarInt().Value;
            int responseCode = response.ReadNextVarInt().Value;
            statusPing = response.ReadNextString().Value;
        }

        public static ServerProviderType CheckProviderFromJar(byte[] jar)
        {
            // todo: validate jar file (CAFEBABE)

            var vanillaSegment = new ArraySegment<byte>(jar, 0x47, 8).ToArray();
            if (Encoding.ASCII.GetString(vanillaSegment) == "META-INF")
                return ServerProviderType.Vanilla;

            var paperSegment = new ArraySegment<byte>(jar, 0x135, 7);
            if (Encoding.ASCII.GetString(paperSegment) == "papermc")
                return ServerProviderType.Paper;

            return ServerProviderType.Custom;
        }
    }
}
